// (c) Czo 2016.
// czo[at]czo[dot]hu
//
#include <sys/sysctl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <syslog.h>
#include <mach/mach.h>

#define DYLD_INTERPOSE(_replacement,_replacee) __attribute__((used)) static struct{ const void* replacement; const void* replacee; } _interpose_##_replacee __attribute__ ((section ("__DATA,__interpose"))) = { (const void*)(unsigned long)&_replacement, (const void*)(unsigned long)&_replacee };

int sysctlbyname_new (const char *name, void *oldp, size_t *oldlenp, void *newp, size_t newlen) {
    char* ret_VMM = "VMM";
    char* ret_model = "iMac10,1";

    if ( strcmp( name, "hw.model" ) == 0 ) {
        *oldlenp = strlen(ret_model) + 1;
        if ( oldp ) {
            strcpy( oldp, ret_model );
        }
        newp = NULL;
        newlen = 0;
        return 0;
    }

    if ( strcmp( name, "machdep.cpu.features" ) == 0 ) {
        *oldlenp = strlen(ret_VMM) + 1;
        if ( oldp ) {
            strcpy( oldp, ret_VMM );
        }
        newp = NULL;
        newlen = 0;
        return 0;
    }


    int reply;
    reply = sysctlbyname (name, oldp, oldlenp, newp, newlen);
    return reply;
}
DYLD_INTERPOSE(sysctlbyname_new, sysctlbyname);

__attribute__((__constructor__)) static void initialize(void) {
}
