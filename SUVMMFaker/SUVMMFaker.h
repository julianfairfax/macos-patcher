//
//  SUVMMFaker.h
//
//  (c) Czo 2017.

#import <Foundation/Foundation.h>

typedef void (^retrieveDistributionDataForProductBlock)(NSObject *url, NSObject *content, NSObject *error ); // return type and parameters are unknown

@interface SUVMMFaker : NSObject
-(void)patchedRetrieveDistributionDataForProduct:(id)arg1 preferredLocalizations:(id)arg2 withHandler:(retrieveDistributionDataForProductBlock)arg3;
@end
