@import Foundation;

// references to old SkyLight
void SLSSessionSwitchToAuditSessionID(int64_t);
void SLSSetDockRectWithReason(int64_t,CGRect,int64_t);
void SLSGetDockRectWithReason(int64_t,CGRect*,int64_t);

// logout and switch-user
void SLSSessionSwitchToAuditSessionIDWithOptions(int64_t sessionID,NSDictionary* transitionStuff)
{
	SLSSessionSwitchToAuditSessionID(sessionID);
}

// references to private HIServices
// stolen from https://github.com/rcarmo/qsb-mac/blob/master/QuickSearchBox/externals/UndocumentedGoodness/CoreDock/CoreDockPrivate.h
typedef enum
{
	kCoreDockOrientationTop=1,
	kCoreDockOrientationBottom=2,
	kCoreDockOrientationLeft=3,
	kCoreDockOrientationRight=4
}
CoreDockOrientation;
typedef enum
{
	kCoreDockPinningStart=1,
	kCoreDockPinningMiddle=2,
	kCoreDockPinningEnd=3
}
CoreDockPinning;
void CoreDockGetOrientationAndPinning(CoreDockOrientation *orientation,CoreDockPinning *pinning);

// Dock collisions
void SLSSetDockRectWithOrientation(int64_t connection,CGRect rect,int64_t reason,int orientation)
{
	// TODO: find a way to store orientation here to avoid getting it from HIServices
	SLSSetDockRectWithReason(connection,rect,reason);
}
void SLSGetDockRectWithOrientation(int64_t connection,CGRect* rect,int64_t reason,CoreDockOrientation* orientation)
{
	SLSGetDockRectWithReason(connection,rect,reason);
	
	CoreDockPinning pinningIgnored;
	CoreDockGetOrientationAndPinning(orientation,&pinningIgnored);
}

// TCC permissions-related functions
// just return true since they weren't restricted in Mojave
// this fixes screen recording access in AltTab and probably other things
bool SLSRequestListenEventAccess()
{
	return true;
}
bool SLSRequestPostEventAccess()
{
	return true;
}
bool SLSRequestScreenCaptureAccess()
{
	return true;
}

// based on class-dump'ed .1 final SkyLight
@interface SLSecureCursorAssertion:NSObject
	@property(retain) NSUUID *uuid;
	@property(readonly,nonatomic,getter=isValid) BOOL valid;
	+(void)invalidateAll;
	+(id)assertion;
	-(void)invalidate;
@end
@implementation SLSecureCursorAssertion
	+(void)invalidateAll
	{
	}
	+(id)assertion
	{
		return nil;
	}
	-(void)invalidate
	{
	}
@end

// based on class-dump'ed DP9 SkyLight
@interface SLDataTimelineConfig:NSObject
	+(id)configWithName:(id)arg1 andUpdateBlock:(void*)arg2;
	-(void)establishConnectionWithResultBlock:(void*)arg1;
	-(id)createXPCObject;
	-(id)createNoSenderRecvPairWithQueue:(id)arg1 errorHandler:(void*)arg2 eventHandler:(void*)arg3;
	-(id)createCancellableMachRecvSourceWithQueue:(id)arg1 cancelAction:(void*)arg2 error:(id *)arg3;
	-(void)requestSampleIntervalValue:(unsigned short)arg1 forKey:(id)arg2;
	-(void)requestReportIntervalValue:(unsigned short)arg1 forKey:(id)arg2;
	-(void)addInfoOption:(id)arg1;
	-(void)setTargetQueue:(id)arg1;
	-(id)initWithName:(id)arg1 andUpdateBlock:(void*)arg2;
@end
@implementation SLDataTimelineConfig
	+(id)configWithName:(id)arg1 andUpdateBlock:(void*)arg2
	{
		return nil;
	}
	-(void)establishConnectionWithResultBlock:(void*)arg1
	{
	}
	-(id)createXPCObject
	{
		return nil;
	}
	-(id)createNoSenderRecvPairWithQueue:(id)arg1 errorHandler:(void*)arg2 eventHandler:(void*)arg3
	{
		return nil;
	}
	-(id)createCancellableMachRecvSourceWithQueue:(id)arg1 cancelAction:(void*)arg2 error:(id *)arg3
	{
		return nil;
	}
	-(void)requestSampleIntervalValue:(unsigned short)arg1 forKey:(id)arg2
	{
	}
	-(void)requestReportIntervalValue:(unsigned short)arg1 forKey:(id)arg2
	{
	}
	-(void)addInfoOption:(id)arg1
	{
	}
	-(void)setTargetQueue:(id)arg1
	{
	}
	-(id)initWithName:(id)arg1 andUpdateBlock:(void*)arg2
	{
		return nil;
	}
@end

// TODO: check the string constants below here (copied from pre-DP7 wrapper)

// copied from new SkyLight
NSString* kSLSSessionSwitchTransitionTypeCube=@"cube";
NSString* kSLSSessionSwitchTransitionTypeKey=@"transition";
NSString* kSLSSessionSwitchTransitionTypeNone=@"none";
NSString* kSLSSessionSwitchTransitionTypeUnset=@"";

// PowerlogLiteOperators
NSString* kSLDataTimelineInfoOptionPerProcessWindows=@"";
NSString* kSLDataTimelineReportIntervalSnapshotCountKey=@"";
NSString* kSLDataTimelineReportIntervalTimeInSecondsKey=@"";
NSString* kSLDataTimelineReportIntervalReportInKiBKey=@"";
NSString* kSLDataTimelineSampleIntervalSnapshotCountKey=@"";
NSString* kSLDataTimelineSampleIntervalTimeInSecondsKey=@"";

// loginwindow
NSString* kSLSCaptureDisablePrompting=@"";
NSString* kSLSCaptureIgnoreTCCPermissions=@"";

// TODO: it's awfully unsettling that changing these to bool functions breaks stuff

// auto-generated stubs
int SLPreflightListenEventAccess()
{
	return 0;
}
int SLPreflightPostEventAccess()
{
	return 0;
}
int SLPreflightScreenCaptureAccess()
{
	return 0;
}
int SLRequestListenEventAccess()
{
	return 0;
}
int SLRequestPostEventAccess()
{
	return 0;
}
int SLRequestScreenCaptureAccess()
{
	return 0;
}
int SLSAAHSetAppearanceTheme()
{
	return 0;
}
int SLSCopyAllSessionPropertiesTemporaryBridge()
{
	return 0;
}
int SLSCopySessionPropertiesTemporaryBridge()
{
	return 0;
}
int SLSDisplayStreamSidecarTouchBar()
{
	return 0;
}
int SLSFindSessionAuditSessionID()
{
	return 0;
}
int SLSGetAppearanceThemeSwitchesAutomatically()
{
	return 0;
}
int SLSGetMenuBarVisibilityOverrideOnDisplay()
{
	return 0;
}
int SLSGetRealtimeDisplayInfoShmem()
{
	return 0;
}
int SLSGetWindowLayerContext()
{
	return 0;
}
int SLSGetZoomDisplay()
{
	return 0;
}
int SLSHWCaptureProcessWindowsInSpaceIncludeDesktop()
{
	return 0;
}
int SLSHWCaptureStreamCreateWithWindow()
{
	return 0;
}
int SLSIsAccessibilityDisplay()
{
	return 0;
}
int SLSIsZoomDisplay()
{
	return 0;
}
int SLSMoveSessionToConsoleTemporaryBridge()
{
	return 0;
}
int SLSPreflightListenEventAccess()
{
	return 0;
}
int SLSPreflightPostEventAccess()
{
	return 0;
}
int SLSPreflightScreenCaptureAccess()
{
	return 0;
}
int SLSReleaseSessionTemporaryBridge()
{
	return 0;
}
int SLSScreenTimeSuppressWindows()
{
	return 0;
}
int SLSSetAppearanceThemeSwitchesAutomatically()
{
	return 0;
}
int SLSSetMenuBarVisibilityOverrideOnDisplay()
{
	return 0;
}
int SLSSetSessionPropertiesTemporaryBridge()
{
	return 0;
}
int SLSSetZoomDisplay()
{
	return 0;
}
int SLSToggleDisplayZoom()
{
	return 0;
}
int SLSecureCursorAssertionCreate()
{
	return 0;
}
int SLSecureCursorAssertionInvalidate()
{
	return 0;
}
int SLSecureCursorAssertionIsValid()
{
	return 0;
}
int SLSessionOwnerCreateWithPort()
{
	return 0;
}
int kSLDisplayStreamGPUBoost=0;
int kSLMenuBarTrackingIsActive=0;
int kSLSDisplayConfigurationTimeline=0;
int kSLSDisplayFadeTimeline=0;
int kSLSDisplayUUIDCallbackTimeline=0;
int kSLSMenuBarHasHiddenUserspaceBarKey=0;
int kSLSScreenTimeAllowDuringLockoutKey=0;
int kSLSScreenTimeSuppressAnimationDurationKey=0;
int kSLSScreenTimeSuppressPIDKey=0;
int kSLSScreenTimeSuppressStateKey=0;
int kSLSSpaceAbsoluteLevelShieldWindow=0;