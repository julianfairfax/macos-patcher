//
//  apfscheck.c
//  macOS Patcher
//
//  Created by Julian Fairfax based on code from https://github.com/dosdude1/macos-catalina-patcher/blob/master/macOS%20Catalina%20Patcher/apfsinsta/APFSManager.m#L115-L137, which is:
//  Copyright (c) 2019 dosdude1 Apps. All rights reserved.
//

#import <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOKitLib.h>

int main(void) {
    io_registry_entry_t romEntry = IORegistryEntryFromPath(kIOMasterPortDefault, "IODeviceTree:/rom@0");
    if (romEntry || (romEntry = IORegistryEntryFromPath(kIOMasterPortDefault, "IODeviceTree:/rom@e0000")) != 0) {
        CFNumberRef apfsProp = IORegistryEntryCreateCFProperty(romEntry, CFSTR("firmware-features"), kCFAllocatorDefault, 0);
        if (!apfsProp) {
            printf("0\n");
        }
        unsigned long long value;
        CFNumberGetValue(apfsProp, kCFNumberSInt64Type, &value);
        CFRelease(apfsProp);
        if ((value & 0x180000) != 0) {
            printf("1\n");
        }
        
    } else {
        printf("0\n");
    }
    printf("0\n");

    return 0;
}